package pl.edu.wat.wel.secon.council_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import java.util.ArrayList;
import java.util.List;

import pl.edu.wat.wel.secon.secon2017.R;

public class CouncilActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CouncilListAdapter adapter;
    private List<CouncilList> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee);

        recyclerView = (RecyclerView) findViewById(R.id.com_recycler_view);

        list = new ArrayList<>();
        adapter = new CouncilListAdapter(list);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);

        //=================================================

        list.add(new CouncilList(getString(R.string.program_committee_header)));
        list.add(new CouncilList("płk. dr hab. inż. Tadeusz SZCZUREK, prof. WAT", getString(R.string.tittle_szczurek_wat), R.drawable.szczurek_rektor));
        list.add(new CouncilList("prof. dr hab. inż. Andrzej DOBROWOLSKI", getString(R.string.tittle_dobrowolski_wat), R.drawable.dobrowolski_dziekan));
        list.add(new CouncilList("doc. dr nw. inż. Artur BAJDA"));
        list.add(new CouncilList("mjr dr inż. Mariusz BEDNARCZYK"));
        list.add(new CouncilList("mgr inż. Rafał BIAŁEK"));
        list.add(new CouncilList("mjr dr inż. Jarosław BUGAJ"));
        list.add(new CouncilList("mgr inż. Jolanta CHMIELIŃSKA"));
        list.add(new CouncilList("ppłk dr inż. Grzegorz CZOPIK"));
        list.add(new CouncilList("mjr dr inż. Mirosław CZYŻEWSKI"));
        list.add(new CouncilList("dr inż. Zbigniew JACHNA"));
        list.add(new CouncilList("płk dr hab. inż. Piotr KANIEWSKI, prof. WAT"));
        list.add(new CouncilList("dr inż. Wojciech KOCAŃDA"));
        list.add(new CouncilList("dr inż. Stanisław KONATOWSKI"));
        list.add(new CouncilList("dr inż. Paweł KWIATKOWSKI"));
        list.add(new CouncilList("mgr inż. Jarosław ŁUKASIAK"));
        list.add(new CouncilList("prof. dr hab. inż. Józef MODELSKI"));
        list.add(new CouncilList("dr hab. inż. Mateusz PASTERNAK, prof. WAT"));
        list.add(new CouncilList("mgr inż. Piotr PAZIEWSKI"));
        list.add(new CouncilList("płk dr hab. inż. Zbigniew PIOTROWSKI, prof. WAT"));
        list.add(new CouncilList("mgr inż. Krzysztof SIECZKOWSKI"));
        list.add(new CouncilList("płk dr inż. Adam SŁOWIK"));
        list.add(new CouncilList("ppłk dr inż. Tadeusz SONDEJ"));
        list.add(new CouncilList("dr inż. Marek SUPRONIUK"));
        list.add(new CouncilList("dr hab. inż. Zbigniew WATRAL, prof. WAT"));
        list.add(new CouncilList("dr inż. Michał WIŚNIOS"));
        list.add(new CouncilList("dr hab. inż. Cezary ZIÓŁKOWSKI, prof. WAT"));

        //==================================================
        adapter.notifyDataSetChanged();

        recyclerView.setAdapter(adapter);
    }
}
