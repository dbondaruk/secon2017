package pl.edu.wat.wel.secon.secon2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class AboutMeActivity extends AppCompatActivity {

    TextView aboutMe1, aboutme2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);

        aboutMe1 = (TextView) findViewById(R.id.about_me_text_view1);
        aboutme2 = (TextView) findViewById(R.id.about_me_text_view2);
        aboutMe1.setText(R.string.about_me1);
        aboutme2.setText(R.string.about_me2);
        aboutMe1.setTextSize(15);
        aboutme2.setTextSize(15);
    }
}
