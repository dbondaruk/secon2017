package pl.edu.wat.wel.secon.secon2017;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class SponsorsActivity extends AppCompatActivity {
    final String websiteTespol = "http://tespol.com.pl";
    final String websiteAMT = "https://www.amt.pl";
    final String websiteFWRRiTM = "http://www.ire.pw.edu.pl/fundacja/";
    final String websiteElektronikaB2B = "http://elektronikab2b.pl";
    final String websiteCzasopismoMienia = "https://www.ochrona-mienia.pl";
    ImageView tespol, amt, fundation, elektronik, elektronikaB2B, czasopismoOchrony;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);

        tespol = (ImageView) findViewById(R.id.sponsors_tespol);
        amt = (ImageView) findViewById(R.id.sponsors_amt);
        fundation = (ImageView) findViewById(R.id.sponsors_wfrritm);
        elektronik = (ImageView) findViewById(R.id.sponsors_elektronik);
        elektronikaB2B = (ImageView) findViewById(R.id.sponsors_elektronika_b2b);
        czasopismoOchrony = (ImageView) findViewById(R.id.sponsors_czasopismo_ochrony);

        onClickImageView(tespol, websiteTespol);
        onClickImageView(amt, websiteAMT);
        onClickImageView(fundation, websiteFWRRiTM);
        onClickImageView(elektronikaB2B, websiteElektronikaB2B);
        onClickImageView(czasopismoOchrony, websiteCzasopismoMienia);
        onClickImageView(elektronik, websiteElektronikaB2B);
    }

    private void onClickImageView(ImageView imageView, final String website) {

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.website_toast_message_error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
