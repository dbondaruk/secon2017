package pl.edu.wat.wel.secon.secon2017;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import pl.edu.wat.wel.secon.council_list.CouncilActivity;
import pl.edu.wat.wel.secon.council_list.OrganizingActivity;


public class MainActivity extends AppCompatActivity {


    // =============================================================================================
    Button buttonOpenContacts, buttonOpenSchedule, buttonOpenMapConference,
            buttonOpenMapParty, buttonOpenProgramCommittee, buttonOpenSponsors;
    double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonOpenContacts = (Button) findViewById(R.id.button_contact);
        buttonOpenSchedule = (Button) findViewById(R.id.button_schedule);
        buttonOpenMapParty = (Button) findViewById(R.id.button_map_party);
        buttonOpenMapConference = (Button) findViewById(R.id.button_map_conference);
        buttonOpenProgramCommittee = (Button) findViewById(R.id.button_program_commission);
        buttonOpenSponsors = (Button) findViewById(R.id.sponsors);

        onClickButton(buttonOpenContacts, ContactActivity.class);
        onClickButton(buttonOpenSchedule, OrganizingActivity.class);
        onClickButton(buttonOpenProgramCommittee, CouncilActivity.class);
        onClickButton(buttonOpenSponsors, SponsorsActivity.class);
        onClickButton(buttonOpenMapConference);
        onClickButton(buttonOpenMapParty);

    }

    /**
     * Metoda przełączająca między aktywnościami, przyjmuje dwa parametry: nazwę przycisku, który
     * obsługuje zdarzenie i aktywność, na którą aplikacja ma się przełączyć.
     *
     * @param b Wybrany przycisk.
     * @param c Aktywność, na którą ma się przełączyć apka.
     */
    public void onClickButton(Button b, final Class c) {
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, c);
                startActivity(intent);
            }
        });
    }

    public void onClickButton(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.button_map_conference: {
                        // Koordynaty dla biblioteki WAT
                        latitude = 52.252966;
                        longitude = 20.895570;
                        String label = getString(R.string.label_secon_place_of_conference);
                        mapIntentExecutor(latitude, longitude, label);
                        break;
                    }
                    case R.id.button_map_party: {
                        // Koordynaty dla imprezy towarzyszącej konferencji
                        latitude = 52.265980;
                        longitude = 20.932924;
                        String label = getString(R.string.label_secon_party);
                        mapIntentExecutor(latitude, longitude, label);
                        break;
                    }
                }
            }
        });
    }

    /**
     * Metoda wykonująca intencję, by otworzyć mapę.
     *
     * @param latitudeLocal  Szerokość geograficzna.
     * @param longitudeLocal Długość geograficzna.
     * @param label          Nazwa miejsca mieszczącego się pod koordynantami.
     */
    public void mapIntentExecutor(double latitudeLocal, double longitudeLocal, String label) {
        String uriBegin = "geo:" + latitudeLocal + "," + longitudeLocal;
        String query = latitudeLocal + "," + longitudeLocal + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), R.string.map_app_avalible_toast_message_error, Toast.LENGTH_SHORT).show();
        }
    }
}




