package pl.edu.wat.wel.secon.council_list;

/**
 * Created by Damian on 2017-04-02
 */

public class CouncilList {

    private static final int NO_IMAGE_PROVIDED = -1;
    private String mFullName;
    private String mTittle;
    private int mImageId;

    public CouncilList(String mFullName) {
        this.mFullName = mFullName;
    }

    public CouncilList(String mTittle, String mFullName) {
        this.mFullName = mFullName;
        this.mTittle = mTittle;
    }

    CouncilList(String mFullName, int mImageId) {
        this.mFullName = mFullName;
        this.mImageId = mImageId;
    }

    public CouncilList(String fullName, String tittle, int imageId) {
        this.mFullName = fullName;
        this.mTittle = tittle;
        this.mImageId = imageId;

    }

    String getFullName() {
        return mFullName;
    }

    String getTittle() {
        return mTittle;
    }

    int getImageId() {
        return mImageId;
    }

    boolean hasImage() {
        return mImageId != NO_IMAGE_PROVIDED;
    }
}
