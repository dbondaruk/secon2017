package pl.edu.wat.wel.secon.council_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.*;
import java.util.List;


import pl.edu.wat.wel.secon.secon2017.*;

public class OrganizingActivity extends AppCompatActivity {

    private CouncilListAdapter adapter;
    private List<CouncilList> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_committee);


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.com_recycler_view);

        list = new ArrayList<>();
        adapter = new CouncilListAdapter(list);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);

        //=================================================

        list.add(new CouncilList(getString(R.string.organizing_committee_header)));
        list.add(new CouncilList("Emilian Suchecki", getString(R.string.head_of_organizing_committee), R.drawable.emilian));
        list.add(new CouncilList("mgr inż. Jarosław Łukasiak", getString(R.string.vce_head_of_organizing_committee), R.drawable.jarek));
        list.add(new CouncilList(getString(R.string.email_committee), "inż. Dariusz Gorgoń\nMichał Kaczorek"));
        list.add(new CouncilList(getString(R.string.invitation_committee), "Damian Bondaruk\nPatryk Konarski\nIza Cylińska-Mysław\ninż. Kamil Miazga\nAleksandra Skuza\nAlan Łopata"));
        list.add(new CouncilList(getString(R.string.graphics), "Marcin Majka\nPiotr Kucal"));
        list.add(new CouncilList(getString(R.string.book), "inż. Michał Kruk"));
        list.add(new CouncilList(getString(R.string.technical_department), "mgr inż. Robert Berczyński\nŁukasz Romanowicz\nMichał Maliszewski\nTomasz Górniak"));
        list.add(new CouncilList(getString(R.string.supply_department), "inż. Marcin Puchlik\ndr inż. Michał Wiśnios"));


        //==================================================
        adapter.notifyDataSetChanged();

        recyclerView.setAdapter(adapter);

    }
}
