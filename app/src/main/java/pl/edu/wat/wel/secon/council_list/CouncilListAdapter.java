package pl.edu.wat.wel.secon.council_list;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import pl.edu.wat.wel.secon.secon2017.R;

/**
 * Created by Damian on 2017-04-02
 */

public class CouncilListAdapter extends RecyclerView.Adapter<CouncilListAdapter.MyViewHolder> {


    private List<CouncilList> itemList;

    public CouncilListAdapter(List<CouncilList> itemList) {
        this.itemList = itemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.com_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final CouncilList list = itemList.get(position);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.tittle.getLayoutParams();

        if (list.getFullName().equals("KOMITET PROGRAMOWY")
                || list.getFullName().equals("PROGRAM COUNCIL")
                || list.getFullName().equals("KOMITET ORGANIZACYJNY")
                || list.getFullName().toLowerCase().equals("organizing committee")) {
            holder.fullName.setTypeface(null, Typeface.BOLD);
            holder.fullName.setTextSize(27);
            params.height = 0;
            holder.tittle.setLayoutParams(params);
            holder.fullName.setGravity(Gravity.CENTER_HORIZONTAL);
        } else {
            holder.fullName.setGravity(Gravity.START);
            holder.fullName.setTextSize(14);
            params.height = 20;
            holder.tittle.setLayoutParams(params);
            holder.fullName.setTypeface(null, Typeface.ITALIC);
        }

        if (list.getTittle() == null) {
            holder.tittle.setText("");

        } else {
            holder.tittle.setTextSize(16);
            holder.fullName.setTypeface(null, Typeface.ITALIC);
            holder.fullName.setGravity(Gravity.CENTER);
            params.height = 240;
            holder.tittle.setLayoutParams(params);
            holder.tittle.setText(list.getTittle());
        }

        holder.fullName.setText(list.getFullName());


        if (list.hasImage()) {
            holder.image.setImageResource(list.getImageId());
            holder.image.setVisibility(View.VISIBLE);
        } else {
            holder.image.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView fullName, tittle;
        ImageView image;

        MyViewHolder(View view) {
            super(view);
            fullName = (TextView) view.findViewById(R.id.com_full_name);
            image = (ImageView) view.findViewById(R.id.com_image);
            tittle = (TextView) view.findViewById(R.id.com_tittle);
        }
    }
}
